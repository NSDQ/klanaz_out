package sk.testy;

import sk.algoritmus.SlovnikovySKanalysis;
import sk.definicie.CommentTest;

public class TestInput2 {
	
	public static void main(String[] args) throws Exception {

		CommentTest ct = new CommentTest(args[0]);
		System.out.println(SlovnikovySKanalysis.analyzuj(ct));

	}
}
