package sk.utils;

import java.util.ArrayList;

import sk.definicie.Prispevok;
import sk.definicie.CommentTest;

public class VytvorPrispevky {
	
	private static int pc;
	private static String subjPrisp;
	private static String autorPrisp;
	private static String textPrispevnku;
	
	static public ArrayList<CommentTest> vytvorZtxt(String obsah) {
		ArrayList<CommentTest> prispevky = new ArrayList<>();
		String[] riadok = obsah.split("\n");
		
		for (String ria : riadok) {				
			try {
				String[] casti = ria.split("\t");
				CommentTest p = null;
				if (casti.length == 4) {
					pc = Integer.parseInt(casti[0]);
					subjPrisp = casti[1];
					autorPrisp = casti[2];
					textPrispevnku = casti[3];
					p = new CommentTest(pc, subjPrisp, autorPrisp, textPrispevnku);
				} else {
					pc = Integer.parseInt(casti[0]);
					subjPrisp = casti[1];
					textPrispevnku = casti[2];
					p = new CommentTest(pc, subjPrisp, textPrispevnku);
				}
				prispevky.add(p);
			} catch (Exception e) {
				System.out.println(pc);
			}
		}
		return prispevky;
	}
	
	static public ArrayList<Prispevok> vytvorZokna(String obsah){
		ArrayList<Prispevok> prispevky = new ArrayList<>();
		String[] zaznamy = obsah.split("\n");
		for (String zaznam : zaznamy) {
			Prispevok p = new Prispevok(zaznam);
			prispevky.add(p);
		}
		return prispevky;
	}
}
