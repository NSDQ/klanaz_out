package sk.utils;

import java.util.ArrayList;

public class MojParser {

	/**
	 * Prechadza vstupny text po znakoch 
	 * a prispevok na vety. Vety od seba oddeluje 
	 * na zaklade znakov: ".?!".
	 * 
	 * @param vstupnyText : text prispevku
	 * @return vrati vety vo formate pola (String[])
	 */
	
	public static ArrayList<String> parsujNaVety(String vstupnyText) {
		
		ArrayList<String> zoznamViet = new ArrayList<>();
		String tempVeta = "" + vstupnyText.charAt(0);
		
		for (int i = 1; i < vstupnyText.length(); i++) {
			if (vstupnyText.charAt(i) != vstupnyText.charAt(i-1)) {
				if (vstupnyText.charAt(i) == '.' || vstupnyText.charAt(i) == '?' 
					|| vstupnyText.charAt(i) == '!') {
					zoznamViet.add(tempVeta);
					tempVeta = "";
				}
				else {
					tempVeta += vstupnyText.charAt(i);
				}
			}
		}
		zoznamViet.add(tempVeta);
		return zoznamViet;
	}
	
	
	
	/**
	 * Vracia <b>OSTEMOVANE</b> slova vety.
	 * 
	 * Prechadza vstupny text po znakoch 
	 * a rozdeli vetu na slova podla zankov: " ,".
	 * Dalej testuje, ci je na akualnej pozici zank {a,z} 
	 * takto ponecha iba slova a odstrani interpunkciu (zatvorky,
	 * dvojbodky, smajliky, ...). Slova vracia v <b>OSTEMOVANOM</b> tvare.
	 * 
	 * @param vstupneVeta : text vety
	 * @return vrati <b>OSTEMOVANE</b> slova vety vo formate pola (String[])
	 */
	
	public static ArrayList<String> parsujNaSlova(String vstupneVeta) {
		
		ArrayList<String> zoznamSlov = new ArrayList<>();
		String tempSlovo = "";
		
		for (int i = 0; i < vstupneVeta.length(); i++) {
			if (vstupneVeta.charAt(i) == ' ' || vstupneVeta.charAt(i) == ',') {
				if (tempSlovo != "") {
					zoznamSlov.add(Stemmer.stemuj(tempSlovo));
					tempSlovo = "";
				}
			}
			else if (vstupneVeta.charAt(i) >= 'a' && vstupneVeta.charAt(i) <= 'z') {
					tempSlovo += vstupneVeta.charAt(i);				
			}
		}
		if (tempSlovo != "") {
			zoznamSlov.add(Stemmer.stemuj(tempSlovo));
		}
		return zoznamSlov;
	}
	
	/*
	private static String[] listToArray(ArrayList<String> list) {
		String[] array = new String[list.size()];
		array = list.toArray(array);
		return array;
	}
	*/
}
