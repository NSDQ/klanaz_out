package sk.utils;

public class UpravDiakritiku {
	
	/**
	 * Zmeni vsetkz pismena na male a odstrani diakritiku.
	 * 
	 * @param text : text, ktory treba upravit
	 * @return upraveny text
	 */
	public static String uprav(String text) {
		
		text = text.toLowerCase();
		text = odstranDiakritiku(text);
		return text;
	}
	
	private static String odstranDiakritiku(String retazec) {
		
		retazec = retazec.replaceAll("ď", "d");
		retazec = retazec.replaceAll("ľ", "l");    
		retazec = retazec.replaceAll("š", "s");    
		retazec = retazec.replaceAll("č", "c");    
		retazec = retazec.replaceAll("ť", "t");    
		retazec = retazec.replaceAll("ž", "z");    
		retazec = retazec.replaceAll("ý", "y");    
		retazec = retazec.replaceAll("á", "a");    
		retazec = retazec.replaceAll("í", "i");    
		retazec = retazec.replaceAll("é", "e");    
		retazec = retazec.replaceAll("ú", "u");    
		retazec = retazec.replaceAll("ä", "a");    
		retazec = retazec.replaceAll("ň", "n");    
		retazec = retazec.replaceAll("ĺ", "l");    
		retazec = retazec.replaceAll("ŕ", "r");    
		retazec = retazec.replaceAll("ô", "o");    
		retazec = retazec.replaceAll("ó", "o");  
	    
		return retazec;
	}
}
