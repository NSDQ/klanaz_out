package sk.utils;

import java.util.HashMap;
import java.util.Map;

import sk.definicie.SlovoSlovnikovy;
import sk.ioOperacie.SlovnikJSON;

public class VytvorSlovnik {
	
	public static Map<String, SlovoSlovnikovy> vytvor() {
		Map<String, SlovoSlovnikovy> slovnik = new HashMap<>();
		slovnik = SlovnikJSON.readSlovnik("C:\\Users\\NSDQ\\workspace\\Vyskum_novy\\res\\dictSK\\json\\SlovnikJSON.json");
		return slovnik;
	}
}
