package sk.utils;

import java.util.ArrayList;

public class VytvorStopSlova {
	
	static public ArrayList<String> vytvor(String obashStopSlova) {
		ArrayList<String> zoznam = new ArrayList<>();
		String[] zoznamStopSlov = obashStopSlova.split("\n");
		for (String string : zoznamStopSlov) {
			zoznam.add(Stemmer.stemuj(UpravDiakritiku.uprav(string)));
		}
		return zoznam;
	}
}
