package sk.utils;

public class UpravDiakritikuCP1250 {
	
	/**
	 * Zmeni vsetkz pismena na male a odstrani diakritiku.
	 * 
	 * @param text : text, ktory treba upravit
	 * @return upraveny text
	 */
	public static String uprav(String text) {
		
		text = text.toLowerCase();
		text = odstranDiakritiku(text);
		return text;
	}
	
	private static String odstranDiakritiku(String retazec) {
	    
		retazec = retazec.replaceAll("�", "d");
		retazec = retazec.replaceAll("�", "l");    
		retazec = retazec.replaceAll("�", "s");    
		retazec = retazec.replaceAll("�", "c");    
		retazec = retazec.replaceAll("�", "t");    
		retazec = retazec.replaceAll("�", "z");    
		retazec = retazec.replaceAll("�", "y");    
		retazec = retazec.replaceAll("�", "a");    
		retazec = retazec.replaceAll("�", "i");    
		retazec = retazec.replaceAll("�", "e");    
		retazec = retazec.replaceAll("�", "u");    
		retazec = retazec.replaceAll("�", "a");    
		retazec = retazec.replaceAll("�", "n");    
		retazec = retazec.replaceAll("�", "l");    
		retazec = retazec.replaceAll("�", "r");    
		retazec = retazec.replaceAll("�", "o");    
		retazec = retazec.replaceAll("�", "o");
		
		return retazec;
	}
}
