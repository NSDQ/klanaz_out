package sk.algoritmus;

import java.util.ArrayList;
import java.util.Map;

import sk.definicie.CommentTest;
import sk.definicie.SlovoSlovnikovy;
import sk.definicie.Veta;
import sk.utils.UpravDiakritiku;
import sk.utils.VytvorSlovnik;

public class SlovnikovySKanalysis {
	
	private static Map<String, SlovoSlovnikovy> slovnik = VytvorSlovnik.vytvor();
	
	public static double analyzuj(CommentTest prispevok) {
		
		double sum = 0.0;
		for (Veta veta : prispevok.getVety()) {
			sum += analyzujNgramy(veta.getSpracovanaVeta());
			sum += analyzujVetu(veta);
			
		}
		//System.err.println(sum);
		return logaritmuj(sum);
	}
	
	public static double analyzuj2(CommentTest prispevok) {
		
		double sum = 0.0;
		for (Veta veta : prispevok.getVety()) {
			sum += analyzujNgramy(veta.getSpracovanaVeta());
			sum += analyzujVetu2(veta);
			
		}
		//System.err.println(sum);
		return logaritmuj(sum);
	}
	
	private static double analyzujNgramy(String veta) {
		
		double sum = 0.0;
		Ngramy ng = new Ngramy();
		sum += ng.hladajNgram(veta);
		return sum;
	}
	
	private static double analyzujVetu(Veta veta) {
		
		//int step = 0;
		double sum = 0.0;
		double hodV,intenz,neg;
		SlovoSlovnikovy slv;
		hodV = 0.0;
		intenz = 1.0;
		neg = 1.0;
		ArrayList<String> slova = veta.getSlova();
		
		for (String slovo : slova) { 			//prehladava slova vety
			if (slovo.length() > 2) {
				slv = najdiVSlovniku(slovo);
				if (slv != null) {
					//System.out.println(slv.getSlovo() + "\t" + slv.getHodn() + "\t" + slv.getSubj());			//vypisuje najdene slova
					/*
					if (step > 3) {
						intenz = 1;
						neg = 1;
						step = 0;
					}
					*/					
					
					switch (slv.getSubj()) {		
						case "p":
							if (slv.getHodn() > 0) {
								if (neg == -1) {
									if (intenz != 1) {
										hodV = hodV + (slv.getHodn() * intenz - 2);
									} else if (slv.getHodn() == 3) {
										hodV = hodV + (slv.getHodn() - 2);
									} else {
										hodV = hodV + (slv.getHodn() * neg);
									}
								} else {
									hodV = hodV + (slv.getHodn() * intenz);
								}
							}
							intenz = 1;
							neg = 1;
							break;
						case "n":
							if (slv.getHodn() < 0) {
								if (neg == -1) {
									if (intenz != 1) {
										hodV = hodV + (slv.getHodn() * intenz + 2);
									} else if (slv.getHodn() <= -3) {
										hodV = hodV + (slv.getHodn() + 2);
									} else {
										hodV = hodV + (slv.getHodn() * neg);
									}
								} else {
									hodV = hodV + (slv.getHodn() * intenz);
								}
							}
							intenz = 1;
							neg = 1;
							break;
						case "i":
							intenz *= slv.getHodn();
							break;
						case "o":
							neg *= -1;					//uprava 3.11.2016
							break;
						default:
							break;
							
							
					}
				}              					//if slv != null
			}									//if(newSlovo.length() > 2)
			//step++;
		}										//for (String slovo : slova)
		sum += hodV;
		return sum;	
	}
	
	private static double analyzujVetu2(Veta veta) {
		
		//int step = 0;
		double sum = 0.0;
		double hodV,intenz,neg;
		SlovoSlovnikovy slv;
		hodV = 0.0;
		intenz = 1.0;
		neg = 1.0;
		ArrayList<String> slova = veta.getSlova();
		
		for (String slovo : slova) { 			//prehladava slova vety
			if (slovo.length() > 2) {
				slv = najdiVSlovniku(slovo);
				if (slv != null) {
					//System.out.println(slv.getSlovo() + "\t" + slv.getHodn() + "\t" + slv.getSubj());			//vypisuje najdene slova
					/*
					if (step > 3) {
						intenz = 1;
						neg = 1;
						step = 0;
					}
					*/
					
					double negVal = 0.0;
					switch (slv.getSubj()) {		
						case "p":
							if (neg == -1) {
								if (intenz != 1) {
									hodV = hodV + (slv.getHodn() * intenz - 2);
								} else if (slv.getHodn() == 3) {
									hodV = hodV + (slv.getHodn() - 2);
								} else {
									hodV = hodV + (slv.getHodn() * neg);
								}
							} else {
								hodV = hodV + (slv.getHodn() * intenz);
							}
							intenz = 1;
							neg = 1;
							break;
						case "n":
							negVal = slv.getHodn();
							if (neg == -1) {
								if (intenz != 1) {
									hodV = hodV + (negVal * intenz + 2);
								} else if (negVal <= -3) {
									hodV = hodV + (negVal + 2);
								} else {
									hodV = hodV + (negVal * neg);
								}
							} else {
								hodV = hodV + (negVal * intenz);
							}
							intenz = 1;
							neg = 1;
							break;
						case "i":
							intenz *= slv.getHodn();
							break;
						case "o":
							neg *= -1;					//uprava 3.11.2016
							break;
						default:
							break;
					}
				}              					//if slv != null
			}									//if(newSlovo.length() > 2)
			//step++;
		}										//for (String slovo : slova)
		sum += hodV;
		return sum;	
	}
	
	private static SlovoSlovnikovy najdiVSlovniku(String slovoVety) {
		if (slovnik.containsKey(slovoVety)) {
			return slovnik.get(slovoVety);
		}
		return null;
	}
	
	private static double logaritmuj(double hodnota) {
		double logHodnota = hodnota;
		if (Math.abs(hodnota) > 1) {
			logHodnota = 1 + Math.log10(Math.abs(hodnota));
			if (hodnota < 0) {
				logHodnota *= -1;
			}
		}
		return logHodnota;
	}
	
	public static void addToDictionary(String slovo, double hodnota) {
		
		SlovoSlovnikovy slvSlv = null;
		slovo = SlovoSlovnikovy.upravNaZakladnyTvar(UpravDiakritiku.uprav(slovo));
		if (hodnota > 0) {
			slvSlv = new SlovoSlovnikovy(slovo, hodnota, "p");
		} else {
			slvSlv = new SlovoSlovnikovy(slovo, hodnota, "n");
		}
		if (slvSlv != null) {
			slovnik.put(slovo, slvSlv);
		}
	}
	
	public static int getDictSize() {
		return slovnik.size();
	}
}
