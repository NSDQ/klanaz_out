package sk.algoritmus;

import java.util.HashMap;
public class Ngramy {
	
	private int sum;
	private HashMap<String, Integer> Ngramy = new HashMap<String, Integer>();	
	
	public Ngramy() {
		Ngramy.put("zmykacka citov", -3);
		Ngramy.put("vyrazi dych", 3);	
		Ngramy.put("chytit za srdce", 3);
		Ngramy.put("hra na city", -3);
		Ngramy.put(":)", 2);
		Ngramy.put(":-)", 2);
		Ngramy.put(":-d", 2);
		Ngramy.put(":-(", -2);
		Ngramy.put(":(", -2);
		Ngramy.put(":-/", -1);
		Ngramy.put(":-*", 3);
		Ngramy.put(":*", 3);
		//Ngramy.put(":-p", 1);
		//Ngramy.put(":o)", 2);
		Ngramy.put("nikdy viac", -3);
		Ngramy.put("vysoka cena", -3);
		Ngramy.put("posobi lacno", -3);
	}
	
	public int hladajNgram(String text) {
		
		sum = 0;
		for (String NgK : Ngramy.keySet()) {
			if (text.contains(NgK)) {
				sum += Ngramy.get(NgK);
			}
		}
		return sum;
	}

}
