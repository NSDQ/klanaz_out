package sk.definicie;

import sk.utils.Stemmer;

public class SlovoSlovnikovy {
	
	private String slovo;
	private double hodn;
	private String subj;
	
	public String getSlovo() {
		return slovo;
	}

	public void setSlovo(String slovo) {
		this.slovo = slovo;
	}

	public double getHodn() {
		return this.hodn;
	}
	
	public void setHodn(double hodnota) {
		this.hodn = hodnota;
	}
	
	public String getSubj() {
		return this.subj;
	}
	
	public void setSubj(String subj) {
		this.subj = subj;		
	}
	
	public SlovoSlovnikovy() {
	}
	
	public SlovoSlovnikovy(String slovo, double hodnota, String subj) {
		this.slovo = slovo;
		this.hodn = hodnota;
		this.subj = subj;
	}
	
	public static String upravNaZakladnyTvar(String slovo) {
		return Stemmer.stemuj(slovo);
	}
}
