package sk.definicie;

import java.util.ArrayList;

import sk.utils.MojParser;
import sk.utils.UpravDiakritiku;

public class Veta {
	
	private final String veta;
	private String spracovanaVeta;
	private ArrayList<String> slova;

	public String getVeta() {
		return veta;
	}
	
	public String getSpracovanaVeta() {
		return spracovanaVeta;
	}

	public void setSpracovanaVeta(String spracovanaVeta) {
		this.spracovanaVeta = spracovanaVeta;
	}

	public ArrayList<String> getSlova() {
		return slova;
	}

	public void setSlova(ArrayList<String> slova) {
		this.slova = slova;
	}

	public Veta(String text) {
		this.veta = text;
		this.spracovanaVeta = UpravDiakritiku.uprav(text);
		this.setSlova(MojParser.parsujNaSlova(spracovanaVeta));
	}
	
	public int vetaSize() {
		return slova.size();
	}
}
