package sk.definicie;

import java.util.ArrayList;

import sk.utils.MojParser;
import sk.utils.UpravDiakritiku;

public class Prispevok implements Comparable<Prispevok>{
	
	private final String text;
	private final String upravText;
	private Double hodSubj = 0.0;
	private ArrayList<Veta> vety = new ArrayList<>();
	
	public String getText() {
		return text;
	}

	public String getUpravText() {
		return upravText;
	}

	public double getHodSubj() {
		return hodSubj;
	}

	public void setHodSubj(double hodSubj) {
		this.hodSubj = hodSubj;
	}

	public ArrayList<Veta> getVety() {
		return vety;
	}

	public Prispevok(String text) {
		this.text = text;
		this.upravText = UpravDiakritiku.uprav(text);
		ArrayList<String> vety = MojParser.parsujNaVety(text);
		for (String v : vety) {
			Veta veta = new Veta(v);
			this.vety.add(veta);
		}
	}
	
	/**
	 * Funkcia vrati zoznam slov vo formate ArrayList&lt;String&gt;.
	 * 
	 * @return ArrayList&lt;String&gt; slova 
	 */
	
	public ArrayList<String> getSlovaPrispeku() {
		
		ArrayList<String> slova	= new ArrayList<>();
		for (Veta v : vety) {
			slova.addAll(v.getSlova());
		}
		return slova;
	}
	
	/**
	 * Funkcia vrati zoznam slov vo formate pola (String[]).
	 * 
	 * @param slova : zoznam slov vo formate ArrayList&lt;String&gt; 
	 * 					idealne P.getSlovaPrispevku
	 * @return String[] slova 
	 */
	
	public String[] toArray(ArrayList<String> slova) {
		String[] slv =  new String[slova.size()];
		slv = slova.toArray(slv);
		return slv;
	}

	public int compareTo(Prispevok o) {
		if (this.hodSubj == o.getHodSubj()) {
			return 1;
		}
		return Double.valueOf(Math.abs(o.getHodSubj())).compareTo(Double.valueOf(Math.abs(this.hodSubj)));
		//return this.hodSubj.compareTo(o.getHodSubj());
	}
	
	public int prispevokSize() {
		int pocet = 0;
		for (Veta veta : vety) {
			pocet += veta.vetaSize();
		}
		return pocet;
	}
}
