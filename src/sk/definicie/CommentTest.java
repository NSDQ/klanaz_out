package sk.definicie;

public class CommentTest extends Prispevok{

	private int pc;
	private String subj;
	private String autor; 
	
	public int getPc() {
		return pc;
	}
	
	public void setPc(int pc) {
		this.pc = pc;
	}
	
	public String getSubj() {
		return subj;
	}
	
	public void setSubj(String subj) {
		this.subj = subj;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}
	
	public CommentTest(String obsah) {
		super(obsah);
		this.pc = 0;
		this.subj = null;
	}

	public CommentTest(int pc, String subj, String obsah) {
		super(obsah);
		this.pc = pc;
		this.subj = subj;
	}
	
	public CommentTest(int pc, String subj, String autor, String obsah) {
		this(pc, subj, obsah);
		this.autor = autor;
	}	
}
