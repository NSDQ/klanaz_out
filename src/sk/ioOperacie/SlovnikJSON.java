package sk.ioOperacie;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import sk.definicie.SlovoSlovnikovy;


public class SlovnikJSON {
	
	public static String PATH = "C:\\Users\\NSDQ\\workspace\\Vyskum_novy\\res\\dictSK\\json\\SlovnikJSON.json";
	private static List<SlovoSlovnikovy> zoznam = new ArrayList<>();
	
	public static Map<String, SlovoSlovnikovy> readSlovnik() {
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			zoznam = mapper.readValue(new File(PATH), new TypeReference<List<SlovoSlovnikovy>>(){});
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listToMap(zoznam);
	}
	
	public static Map<String, SlovoSlovnikovy> readSlovnik(String path) {
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			zoznam = mapper.readValue(new File(path), new TypeReference<List<SlovoSlovnikovy>>(){});
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listToMap(zoznam);
	}
	
	public static void writeSlovnik(Map<String, SlovoSlovnikovy> zoznam) {
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			mapper.writeValue(new File(PATH), new ArrayList<SlovoSlovnikovy>(zoznam.values()));
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeSlovnik(Map<String, SlovoSlovnikovy> zoznam, String path) {
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			mapper.writeValue(new File(path), new ArrayList<SlovoSlovnikovy>(zoznam.values()));
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Map<String, SlovoSlovnikovy> deleteSlovo(Map<String, SlovoSlovnikovy> slovnik, SlovoSlovnikovy slovo) {
		
		if (slovnik.remove(slovo.getSlovo()) == null) {
			JOptionPane.showMessageDialog(null, "Problem pri mazani slova. "
					+ "Slovo sa asi v slovniku nenachadza \n", "Chyba", JOptionPane.ERROR_MESSAGE);
		}
		return slovnik;
	}
	
	public static Map<String, SlovoSlovnikovy> addSlovo(Map<String, SlovoSlovnikovy> slovnik, SlovoSlovnikovy slovo) {
		
		if (slovnik.put(slovo.getSlovo(), slovo) != null) {
			JOptionPane.showMessageDialog(null, "Problem pri pridavani slova. "
					+ "Slovo sa asi uz v slovniku nachadza \n", "Chyba", JOptionPane.ERROR_MESSAGE);
		}
		return slovnik;
	}
 
	private static Map<String, SlovoSlovnikovy> listToMap (List<SlovoSlovnikovy> zoznam) {
		
		Map<String, SlovoSlovnikovy> z =  new HashMap<>();
		for (SlovoSlovnikovy slovo : zoznam) {
			z.put(slovo.getSlovo(), slovo);
		}
		return z;
	}
}
